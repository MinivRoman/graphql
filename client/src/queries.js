import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery($filter: String, $skip: Int, $first: Int, $orderBy: MessageOrderByInput) {
  messages(filter: $filter, skip: $skip, first: $first, orderBy: $orderBy) {
    messageList {
      id
      text
      like
      dislike
      replies {
      	id
        text
      }
    }
    count
  }
}
`;

export const POST_MESSAGE_MUTATION = gql`
mutation PostMutation($text: String!, $like: Int!, $dislike: Int!) {
  postMessage(text: $text, like: $like, dislike: $dislike) {
    id
    text,
		like,
    dislike,
    replies {
      id
    }
  }
}
`;

export const UPDATE_MESSAGE_MUTATION = gql`
mutation UpdateMutation($id: ID!, $like: Int!, $dislike: Int!) {
  updateMessage(id: $id, like: $like, dislike: $dislike) {
    id
    text,
		like,
    dislike
  }
}
`;

export const POST_REPLY_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $text: String!) {
    postReply(messageId: $messageId, text: $text) {
      id
      text
    }
  }
`;

export const MESSAGES_SUBSCRIPTION = gql`
  subscription {
  message {
    id
    text
    like
    dislike
    replies {
      id
      text
    }
  }
}
`;

export const NEW_REPLIES_SUBSCRIPTION = gql`
  subscription {
  newReply {
    id
    text
  }
}
`;