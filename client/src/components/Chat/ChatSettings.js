import React from 'react';
import { Form } from 'semantic-ui-react';

class ChatSettings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sortType: 'createdAt',
            sortOrder: false,
            messageText: ''
        };
    }
    
    handleChangeSortBy = (e, { value }) => {
        this.setState({ sortType: value });
        this.props.handleMessageSettings({ sortType: value });
    };
    handleChangeSortOrder = (e, { checked }) => {
        this.setState({ sortOrder: checked });
        this.props.handleMessageSettings({ sortOrder: checked });
    };
    handleChangeMessageText = (e, { value }) => {
        this.setState({ messageText: value });
        this.props.handleMessageSettings({ messageText: value });
    };

    render() {

        return (
            <div className='chat-settings'>
                <Form>
                    <Form.Group inline>
                        <label>Sort by:</label>
                        <Form.Radio
                            label='Created at'
                            value='createdAt'
                            checked={this.state.sortType === 'createdAt'}
                            onChange={this.handleChangeSortBy}
                        />
                        <Form.Radio
                            label='Like'
                            value='like'
                            checked={this.state.sortType === 'like'}
                            onChange={this.handleChangeSortBy}
                        />
                        <Form.Radio
                            label='Dislike'
                            value='dislike'
                            checked={this.state.sortType === 'dislike'}
                            onChange={this.handleChangeSortBy}
                        />
                        <Form.Checkbox
                            label='Reverse'
                            checked={this.state.sortOrder}
                            onChange={this.handleChangeSortOrder}
                        />
                    </Form.Group>
                    <Form.Group inline>
                        <Form.TextArea
                            label='Filter by:'
                            placeholder='Message text...'
                            value={this.state.messageText}
                            onChange={this.handleChangeMessageText}
                        />
                    </Form.Group>
                </Form>
            </div>
        );
    }
}

export default ChatSettings;