import React from 'react';
import { Pagination } from 'semantic-ui-react';

class ChatPagination extends React.Component {
    render() {
        return (
            <div className='chat-pagination'>
                <Pagination
                    onPageChange={this.props.handlePageChange}
                    boundaryRange={0}
                    // defaultActivePage={1}
                    ellipsisItem={null}
                    firstItem={null}
                    lastItem={null}
                    siblingRange={1}
                    totalPages={this.props.totalPages}
                    activePage={this.props.activePage}
                />
            </div>
        );
    }
}

export default ChatPagination;