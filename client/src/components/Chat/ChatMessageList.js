import React from 'react';
import { Query } from 'react-apollo';
import { MESSAGE_QUERY, MESSAGES_SUBSCRIPTION } from '../../queries';
import ChatMessage from './ChatMessage';
import ChatPagination from './ChatPagination';

class ChatMessageList extends React.Component {
    _subscribeToNewMessages = subscribeToMore => {
        subscribeToMore({
            document: MESSAGES_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                
                const { message } = subscriptionData.data;
                const exists = prev.messages.messageList.find(({ id }) => id === message.id);
                if (exists) return prev;

                return {
                    ...prev, messages: {
                        messageList: [message, ...prev.messages.messageList],
                        count: prev.messages.messageList.length + 1,
                        __typename: prev.messages.__typename
                    }
                };
            }
        });
    };
    
    render() {
        const
            { orderBy, filter } = this.props.settings,
            { activePage } = this.props,
            skip = (activePage - 1) * 5,
            first = 5;
        return (
            <Query query={MESSAGE_QUERY} variables={{ filter, skip, first, orderBy }}>
                {({ loading, error, data, subscribeToMore }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Fetch error</div>;

                    this._subscribeToNewMessages(subscribeToMore);
                    
                    const totalPages = Math.ceil(data.messages.count / 5);
                    const { messages: { messageList } } = data;
                    
                    return (
                        <div className="chat-message-list">
                            {messageList.map((message) => <ChatMessage
                                key={message.id}
                                id={message.id}
                                text={message.text}
                                like={message.like}
                                dislike={message.dislike}
                                replies={message.replies}
                            />)}
                            <ChatPagination
                                activePage={this.props.activePage}
                                handlePageChange={this.props.handlePageChange}
                                totalPages={totalPages}
                            />
                        </div>
                    );

                }}
            </Query>
        );
    }
}

export default ChatMessageList;