import React from 'react';
import './Chat.css';
import ChatSettings from './ChatSettings';
import ChatMessageList from './ChatMessageList';
import ChatMessageInput from './ChatMessageInput';

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            settings: {
                filter: '',
                orderBy: 'createdAt_DESC'
            },
            activePage: 1
        };
    }

    handlePageChange = (e, { activePage }) => {
        this.setState({ activePage });
    }

    handleMessageSettings = ({
        sortType = this.state.settings.orderBy.split('_')[0],
        sortOrder = false,
        messageText = ''
    }) => {
        const settings = {
            filter: messageText,
            orderBy: `${sortType}_${sortOrder ? 'ASC' : 'DESC'}`
        }
        this.setState({ settings });
    }

    render() {
        return (
            <div className='chat'>
                <ChatSettings
                    handleMessageSettings={this.handleMessageSettings}
                />
                <ChatMessageList
                    settings={this.state.settings}
                    activePage={this.state.activePage}
                    handlePageChange={this.handlePageChange}
                />
                <ChatMessageInput
                    settings={this.state.settings}
                />
            </div>
        );
    }
}

export default Chat;