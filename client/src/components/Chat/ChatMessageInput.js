import React from 'react';
import { Form, Button } from 'semantic-ui-react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from '../../queries';

class ChatMessageInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messageText: ''
        };
    }

    handleChangeMessageText = (e, { value }) => this.setState({ messageText: value });

    render() {
        return (
            <div className='chat-message-input'>
                <Form reply>
                    <Form.TextArea value={this.state.messageText} onChange={this.handleChangeMessageText} />
                    <Mutation
                        mutation={POST_MESSAGE_MUTATION}
                        variables={{ text: this.state.messageText, like: 0, dislike: 0 }}
                        update={(store, { data: { postMessage } }) => {
                            this.setState({ messageText: '' });
                        }}
                    >
                        {postMutation =>
                            <Button content='Add Comment' labelPosition='left' icon='edit' primary onClick={postMutation} />
                        }
                    </Mutation>
                </Form>
            </div>
        );
    }
}

export default ChatMessageInput;