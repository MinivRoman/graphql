import React from 'react';
import { Comment, Label } from 'semantic-ui-react';

class ChatMessageList extends React.Component {
    render() {
        return (
            <div className='chat-message-reply'>
                <Comment.Group>
                    <Comment>
                        <Comment.Content>
                            <Label className='message-id'>{this.props.id}</Label>
                            <Comment.Text>
                                {this.props.text}
                            </Comment.Text>
                        </Comment.Content>
                    </Comment>
                </Comment.Group>
            </div>
        );
    }
}

export default ChatMessageList;