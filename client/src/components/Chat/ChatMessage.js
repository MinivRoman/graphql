import React from 'react';
import { Button, Comment, Icon, Label, Form } from 'semantic-ui-react';
import ChatMessageReply from './ChatMessageReply';
import { Mutation } from 'react-apollo';
import { POST_REPLY_MUTATION, UPDATE_MESSAGE_MUTATION } from '../../queries';

class ChatMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            replyForm: false,
            messageReply: ''
        };
    }

    handleClickReply = (e) => {
        if (this.state.replyForm && !this.state.messageReply) {
            return;
        }

        this.setState({ replyForm: !this.state.replyForm });
    };

    handleChangeMessageReply = (e, { value }) => this.setState({ messageReply: value });

    render() {
        return (

            <div className='chat-message'>
                <Comment.Group>
                    <Comment>
                        <Comment.Content>
                            <Label className='message-id'>{this.props.id}</Label>
                            <Comment.Metadata>
                                <div>
                                    <Mutation
                                        mutation={UPDATE_MESSAGE_MUTATION}
                                        variables={{ id: this.props.id, like: this.props.like + 1, dislike: this.props.dislike }}
                                    >
                                        {updateMutation =>
                                            <Button onClick={updateMutation}>
                                                <Icon name="thumbs up outline" />
                                                {this.props.like}
                                            </Button>
                                        }
                                    </Mutation>
                                    <Mutation
                                        mutation={UPDATE_MESSAGE_MUTATION}
                                        variables={{ id: this.props.id, like: this.props.like, dislike: this.props.dislike + 1 }}
                                    >
                                        {updateMutation =>
                                            <Button onClick={updateMutation}>
                                                <Icon name="thumbs down outline" />
                                                {this.props.dislike}
                                            </Button>
                                        }
                                    </Mutation>
                                </div>
                            </Comment.Metadata>
                            <Comment.Text>
                                {this.props.text}
                            </Comment.Text>
                            <Comment.Actions>
                                <Comment.Action onClick={this.handleClickReply}>Reply</Comment.Action>
                            </Comment.Actions>
                            {
                                this.state.replyForm ?
                                    <Form reply success={false}>
                                        <Form.TextArea value={this.state.messageReply} onChange={this.handleChangeMessageReply} />
                                        <Mutation
                                            mutation={POST_REPLY_MUTATION}
                                            variables={{ messageId: this.props.id, text: this.state.messageReply }}
                                            update={(store, { data: { postMessage } }) => {
                                                this.setState({ messageReply: '', replyForm: !this.state.replyForm });
                                            }}
                                        >
                                            {postMutation =>
                                                <Button content='Add Reply' labelPosition='left' icon='edit' primary onClick={postMutation} />
                                            }
                                        </Mutation>
                                    </Form> : null
                            }
                            {this.props.replies.map((reply) => <ChatMessageReply
                                key={reply.id}
                                id={reply.id}
                                text={reply.text}
                            />)}
                        </Comment.Content>
                    </Comment>
                </Comment.Group>
            </div>
        );
    }
}

export default ChatMessage;