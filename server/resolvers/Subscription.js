const messageSubscribe = (parent, args, context, info) => {
    return context.prisma.$subscribe.message({
        mutation_in: ['CREATED', 'UPDATED']
    }).node();
}

const message = {
    subscribe: messageSubscribe,
    resolve: payload => {
        return payload;
    }
};

const newReplySubscribe = (parent, args, context, info) => {
    return context.prisma.$subscribe.reply({
        mutation_in: ['CREATED']
    }).node();
}

const newReply = {
    subscribe: newReplySubscribe,
    resolve: payload => {
        return payload;
    }
};

module.exports = {
    message,
    newReply
}