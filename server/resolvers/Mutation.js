const postMessage = (parent, args, context, info) => {
    return context.prisma.createMessage({
        text: args.text,
        like: args.like,
        dislike: args.dislike
    });
}

const updateMessage = (parent, args, context, info) => {
    return context.prisma.updateMessage({
        where: {
            id: args.id
        },
        data: {
            like: args.like,
            dislike: args.dislike
        }
    });
}

const postReply = async (parent, args, context, info) => {
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });

    if (!messageExists) {
        throw new Error(`Message with ID ${args.messageId} does not exist`);
    }

    return context.prisma.createReply({
        text: args.text,
        message: {
            connect: {
                id: args.messageId
            }
        }
    })
}

module.exports = {
    postMessage,
    updateMessage,
    postReply
}